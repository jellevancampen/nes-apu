#include <stdio.h>
#include <inttypes.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "esp_timer.h"
#include "sdkconfig.h"

#define INCLUDE_vTaskDelayUntil 1
#define configUSE_TIMERS 1
#define configTIMER_SERVICE_TASK_NAME "TMR_SVC"
#define configTIMER_TASK_PRIORITY (configMAX_PRIORITIES - 1)
#define configTIMER_TASK_STACK_DEPTH (configMINIMAL_STACK_SIZE * 2)
#define configTIMER_QUEUE_LENGTH 10
#define configTICK_RATE_HZ 240

#define NES_CPU_HZ 1789773

// Pulse01 registers.
uint8_t reg4000 = 0b00000000; // DDLC NNNN (duty, loop envelope/disable length counter, enable constant volume, envelope period/volume).
uint8_t reg4001 = 0b00000000; // EPPP NSSS (enable sweep, sweep period, negative sweep, sweep shift count).
uint8_t reg4002 = 0b00000000; // LLLL LLLL (timer period low).
uint8_t reg4003 = 0b00000000; // LLLL LHHH (length counter load index, timer period high (+ reset duty and start envelope)).

// Pulse02 registers.
uint8_t reg4004 = 0b00000000; // DDLC NNNN (duty, loop envelope/disable length counter, enable constant volume, envelope period/volume).
uint8_t reg4005 = 0b00000000; // EPPP NSSS (enable sweep, sweep period, negative sweep, sweep shift count).
uint8_t reg4006 = 0b00000000; // LLLL LLLL (timer period low).
uint8_t reg4007 = 0b00000000; // LLLL LHHH (length counter load index, timer period high (+ reset duty and start envelope)).

// Triangle registers.
uint8_t reg4008 = 0b00000000; // CRRR RRRR (length counter disable/linear counter control, linear counter reload value)
uint8_t reg400A = 0b00000000; // LLLL LLLL (timer period low).
uint8_t reg400B = 0b00000000; // LLLL LHHH (length counter load index, timer period high (+ reload linear counter))

// Noise registers.
uint8_t reg400C = 0b00000000; // --LC NNNN (loop envelope/disable length counter, enable constant volume, envelope period/volume).
uint8_t reg400E = 0b00000000; // L--- PPPP (loop noise, noise period index).
uint8_t reg400F = 0b00000000; // LLLL L--- (length counter load index (+ start envelope)).

// DMC registers.
uint8_t reg4010 = 0b00000000; // IL-- FFFF (IRQ enable, enable loop sample, frequency index).
uint8_t reg4011 = 0b00000000; // -DDD DDDD (direct load DAC).
uint8_t reg4012 = 0b00000000; // AAAA AAAA (sample address %11AAAAAA.AA0000000).
uint8_t reg4013 = 0b00000000; // LLLL LLLL (sample lenght %0000LLLL.LLLL00010).

// Control registers.
uint8_t reg4015 = 0b00000000; // Write: ---D NT21 (enable DMC, enable length counters: noise, triangle, pulse02, pulse01). 
                              // Read:  IF-D NT21 (DMC IRQ, frame IRQ, DMC enable status, enable length counter statuses: noise, triangle, pulse02, pulse01).
uint8_t reg4017 = 0b00000000; // SD-- ---- (enable 5-frame sequence, disable frame IRQ).

uint8_t duty_cycle_sequences[] = { 0b00000001, 0b00000011, 0b00001111, 0b11111100 };
esp_timer_handle_t pulse01_timer = 0; // 
uint8_t pulse01_sequencer = 0;

// Task handles.
TaskHandle_t task_frame_update;

// Helper functions.
static inline uint64_t nes_timer_to_microseconds(uint16_t t) { return (t + 1) * 2000000 / NES_CPU_HZ; }
static inline uint16_t nes_timer_from_high_low_registers(uint8_t regHigh, uint8_t regLow) { return ((regHigh & 0b00000111) << 8) + regLow;}
static inline bool sequence_duty_cycle_pattern(uint8_t sequence_index, uint8_t bit_index) { return (duty_cycle_sequences[sequence_index] >> bit_index) & 0b00000001; }

// Update pulse01 channel with a periodicity based on pulse01 timer. 
static void update_pulse01_channel(void* params)
{
        //printf("Seq: %i, DC: %i, value: %i\n", pulse01_sequencer, reg4000 >> 6, sequence_duty_cycle_pattern(reg4000 >> 6, pulse01_sequencer));
        gpio_set_level(25, sequence_duty_cycle_pattern(reg4000 >> 6, pulse01_sequencer));
        if (pulse01_sequencer == 0) { pulse01_sequencer= 7; } else { --pulse01_sequencer; } 

        // Reschedule the timer for the next channel update. 
        esp_timer_stop(pulse01_timer);
        esp_timer_start_once(pulse01_timer, nes_timer_to_microseconds(nes_timer_from_high_low_registers(reg4003, reg4002)));
}

void run_task_test(void *pvParameters)
{
        TickType_t lastWakeTime = xTaskGetTickCount();
        //const TickType_t taskPeriod = 1;
        const TickType_t taskPeriod = 240;

        for(;;)
        {
                for(int i=0; i<10; i++)
                {
                        if (reg4002 < 0b11111111)
                        {
                                reg4002++;
                        }
                        else
                        {
                                reg4002 = 0;
                                if (reg4003 < 0b00000111) 
                                {
                                        reg4003++;
                                }
                                else
                                {
                                        reg4003 = 0;
                                }
                                
                        }        
                }
                

                if ((reg4000 >> 6) < 3) 
                {
                        reg4000 += 1 << 6;
                }
                else
                {
                        reg4000 = 0;
                }
                
                
                vTaskDelayUntil(&lastWakeTime, taskPeriod);
        }
}

void run_task_timer(void *pvParameters)
{
      
}

void app_main() 
{
        gpio_pad_select_gpio(25);
        gpio_set_direction(25, GPIO_MODE_OUTPUT);

        reg4002 = 32;

        // Create and start timers for channel updates. 
        const esp_timer_create_args_t pulse01_timer_args = { .callback = &update_pulse01_channel, .name = "tmr_pulse01" };
        ESP_ERROR_CHECK(esp_timer_create(&pulse01_timer_args, &pulse01_timer));
        ESP_ERROR_CHECK(esp_timer_start_once(pulse01_timer, 0));

        // Create and start task for frame update.
        BaseType_t result = xTaskCreate(run_task_test, "test_task", 10000, (void*) 1, tskIDLE_PRIORITY + 1, &task_frame_update);
        printf("%i (PDPass = %i)\n", result, pdPASS);
}